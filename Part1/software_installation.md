# Software installation

We need to create a separate virtual environment using Anaconda package of python and install software __VSEARCH__ and a package __Biopython__ in this environment.  
To do so, in a __terminal__ window, paste the following comand using __CTRL+SHIFT+V__ and press __Enter__:  
  
>conda create -n vsearch -c bioconda -c anaconda vsearch fastqc
  
If there are any questions in terminal, type `y` and press __Enter__.  
After this, you should be able to activate and deactivate the new environment:  
  
>conda activate vsearch  

If it does not work, try to run the following line instead:  
>source activate vsearch  

Now, install package __Biopython__ with the following command:

>pip3 install biopython   
