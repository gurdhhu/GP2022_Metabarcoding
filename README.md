# Grosspraktikum 2022/23: DNA Metabarcoding

This course material is designed to work on resources of University of Greifswald.  

You have to be connected to the intranet of the University (establish VPN connection if necessary, see https://rz.uni-greifswald.de/dienste/technische-infrastruktur/vpn/).  

All analyses are executed remotely at the University servers via web browser. There is no need ot install anything on your PC / paptop.

## Clone course materials

Open a new __terminal__ window (black symbol with white dollar-underscore sign), paste the following comand using __CTRL+SHIFT+V__ and press __Enter__:

>git clone https://gitlab.com/gurdhhu/GP2022_Metabarcoding.git

On the left, a folder `GP2022_Metabarcoding` will appear in your "file navigator". 

## Course structure

Part 1: Software Installation and Downloading the Data  

Part 2: Brocessing Raw Illumina Data  

Part 3: Statistical Analysis  
