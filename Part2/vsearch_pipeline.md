# VSEARCH pipeline for metabarcoding data

All the commands provided below should be executed in the __terminal__ using copy __(CTRL+C)__ and paste __(CTRL+SHIFT+V)__.

## 1. Activate the environment and go to the project directory

****
```bash
conda activate vsearch  # activate the environment
# If it does not work, try to run the following line (without # sign!)
# source activate vsearch
    
cd ./GP2022_Metabarcoding/Part2  # Go to the project directory
```
****

## 2. Take a look inside a FATSQ file
FASTQ format is a raw sequencing data format that you usually receive from high-throughput sequencing devices. Its is basically a text.  
Run the command below to see the first 10 lines of one of the FASTQ files.
****
```bash  
gzip -cd ./DATA/Run20211025Order279Sample003_2S1_L001_R1_001.fastq.gz | head
    
```
****

## 3. Check sequence quality using FASTQC
Run the command below. When it finishes, check the contents of the directory /GP2022_Metabarcoding/Part2/fastqc_reports. Open one or several of the HTML files there.
****
```bash  
mkdir fastqc_reports
for f in ./DATA/*.fastq.gz; do  # start a cycle to process every FASTQ file

    fastqc $f -o ./fastqc_reports -t 6

done
    
```
****

## 4. Provide necessary variables
Instead of typing long paths to files or providing the same parameters manually to each command, we can store them as variables and call them every time we need.    
****
```bash
THREADS=12  # Select a desired number of threads for parallelization

REF=./DBs/Myxomycete_references_20221025.fas # reference collection of myxomycete SSU sequences
    
EUKREF=./DBs/SILVA_90_references.fas  # reference collection of eukaryotic SSU sequences (downloaded from https://www.arb-silva.de/)
```
****
## 5. Convert reference sequence databases into binary format
This step creates binary databases to speed up sequence search in further steps. This needs to be done only once for each database.  

****
```
vsearch --makeudb_usearch $REF --dbmask none --output $REF.udb
vsearch --makeudb_usearch $EUKREF --dbmask none --output $EUKREF.udb
```
****
## 6. Process all raw FASTQ files one by one
The commands below start a cycle for each pair of FASTQ files:  
- forward and reverse reads will be merged and saved in FASTQ format;
- low-quality reads will be removed and remaining reads saved in FASTA format;
- reads will be dereplicated (only one copy of each unique sequence will be preserved, and information about its abundance in a sample will be added to its title).

****
```shell
mkdir output
for f in ./DATA/*_R1_*.fastq.gz; do  # start a cycle to process every forward FASTQ file

    r=$(sed -e "s/_R1_/_R2_/" <<< $f)  # save a name of the reverse read as variable "r".

    s=$(awk -F'[_]' '{print $2}' <<< $f)  # save a name of currently processed sample as "s". The program "awk" splits file name by "_" symbol and takes the second resulting element. For example, for file name "Run20211025Order279Sample003_2S1_L001_R1_001.fastq.gz" it is "2S1".

	echo
	echo ====================================
	echo Processing sample $s  # The command "Echo" prints into terminal the text that follows it.
	echo ====================================

	# here, the forward and reverse reads are merged.
	# fastq_mergepairs - the command itself, followed by the path to the file with forward reads (f)
	# threads - desired number of parallel processes
	# reverse - path to the file with reverse reads (r)
	# fastq_minovlen - minimum allowed lengh of overlapping region of forward and reverse reads
	# fastq_maxdiffs - maximum number of allowed mismatches in overlapping region
	# fastqout - path for the output FASTQ file with merged reads
	# fastq_eeout - add the estimated expected number of errors to the read name

    vsearch --fastq_mergepairs $f \
    	--threads $THREADS \
        --reverse $r \
        --fastq_minovlen 50 \
        --fastq_maxdiffs 20 \
        --fastqout ./output/$s.merged.fastq \
        --fastq_eeout

    echo
    echo Calculate quality statistics

    # Here, some quality statistics for merged reads will be calculated. Thy can help adjusting the parameters for quality filtration.

    vsearch --fastq_eestats ./output/$s.merged.fastq \
        --output ./output/$s.stats

    echo
    echo Quality filtering

    # Remove low-quality reads from the sample.
    # fastq_filter - the command itself, followed by the path to the file with merged sequences.
    # fastq_maxee - maximum allowed expected number of errors in a sequence
    # fastq_minlen - minimum allowed sequence length
    # fastq_maxns - max allowed number of uncertain bases (Ns)
    # fastaout - path for the output FASTA file
    # fasta_width - length of the lines for the ouptut FASTA file. 0 = no line splitting
    # fastq_stripleft/stripright - how many characters to strip from both ends to remove primer regions.

    vsearch --fastq_filter ./output/$s.merged.fastq \
        --fastq_maxee 0.5 \
        --fastq_minlen 200 \
        --fastq_maxns 0 \
        --fastaout ./output/$s.filtered.fasta \
        --fasta_width 0 \
        --fastq_stripleft 19 \
        --fastq_stripright 23

    echo
    echo Dereplicate at sample level and relabel with sample_n

    # Sequence dereplication.
    # derep_fulllength - the command itself, followed by the path to the file with quality-filtered sequences
    # strand - "both" to merge complementary sequences
    # output - path for the output FASTA file
    # sizeout - preserve the original abundance of sequence as part of the sequence name
    # uc - path to the output "map" of sequences in UC format
    # relabel - give a new name (here - sample name) to each sequence followed by 1, 2, 3 etc.

    vsearch --derep_fulllength ./output/$s.filtered.fasta \
        --strand both \
        --output ./output/$s.derep.fasta \
        --sizeout \
        --uc ./output/$s.derep.uc \
        --relabel $s. \
        --fasta_width 0

done # End of the cycle
    
```
****

## 7. Pool all samples together and dereplicate globally
  
****
```bash
# Merge all samples
cat ./output/*.derep.fasta > ./output/all.fasta  # Pool sequences from all samples together

# Dereplicate across samples and remove singletons
# minuniquesize - minimum allowed abundance of sequence. If 2, all singletons will be removed. 
# sizein - sequence abundance information stored in the sequence name will be taken into account

vsearch --derep_fulllength ./output/all.fasta \
    --strand both \
    --minuniquesize 10 \
    --sizein \
    --sizeout \
    --fasta_width 0 \
    --uc ./output/all.derep.uc \
    --output ./output/all.derep.fasta

echo Unique non-singleton sequences: $(grep -c "^>" ./output/all.derep.fasta)

```
****

## 8. "Denoise" sequences
In this step, an algorithm will try to get rid of erros that occurred during PCR. Such erroneous reads usually differ in 1-2 bases from "true" reads and are much less abundant than "true" reads.  
Sequences that survived denoising are called ASVs - Amplicon Sequence Variants. They are treated as taxonomical units.  
Resulting sequences are sorted by abundance (most abundant go first).

****
```bash
vsearch --cluster_unoise ./output/all.derep.fasta \
    --threads $THREADS \
    --fasta_width 0 \
    --sizein \
    --sizeout \
    --minsize 10 \
    --unoise_alpha 2.0 \
    --uc ./output/denoised.uc \
    --centroids ./output/centroids.fasta

echo Unique ASVs after denoising: $(grep -c "^>" ./output/centroids.fasta)

# Sort ASVs by decreasing abundance

vsearch --sortbysize ./output/centroids.fasta \
    --sizein \
    --sizeout \
    --fasta_width 0 \
    --output ./output/sorted.fasta
```
****

## 9. Chimera detection
Two chimera detection algorithms will be applied:
- De novo chimera detection - based on comparison of analyzed sequences with each other;
- Reference-based detection - based on comparison of analyzed sequences with reference sequences.

****
```bash
vsearch --uchime3_denovo ./output/sorted.fasta \
    --sizein \
    --sizeout \
    --fasta_width 0 \
    --qmask none \
    --nonchimeras ./output/denovo.nonchimeras.fasta \

echo ASVs after de novo chimera detection: $(grep -c "^>" ./output/denovo.nonchimeras.fasta)

vsearch --uchime_ref ./output/denovo.nonchimeras.fasta \
	--threads $THREADS \
    --db $REF \
    --sizein \
    --sizeout \
    --fasta_width 0 \
    --qmask none \
    --dbmask none \
    --nonchimeras ./output/nonchimeras.fasta

echo Unique sequences after reference-based chimera detection: $(grep -c "^>" ./output/nonchimeras.fasta)

```
****

## 10. Relabel ASVs and create ASV table
In this step, each sequence that has survived previous steps gets labelled as ASV1, ASV2 etc.  
After that, a table of distribution of these ASVs over samples is created by aligning quality-filtered reads from each sample to the ASVs. 
****
```bash
# Relabel ASVs
vsearch --fastx_filter ./output/nonchimeras.fasta \
    --sizein \
    --sizeout \
    --fasta_width 0 \
    --relabel ASV \
    --fastaout ./output/ASVs.fasta

echo Total number of ASVs: $(grep -c "^>" ./output/ASVs.fasta)

# Map sequences to ASVs by searching
# usearch_global - the command itself, followed by a path to quality-filtered sequences pooled from all samples (but not yet dereplicated globally)
# db - path to the file with ASVs
# id - sequence similarity threshold (from 0 to 1)
# otutabout - path for the output ASV table in TSV format - tab-separated value, can be opened in Excel or other spreadsheet program.

vsearch --usearch_global ./output/all.fasta \
    --threads $THREADS \
    --db ./output/ASVs.fasta \
    --id 0.99 \
    --strand both \
    --sizein \
    --sizeout \
    --fasta_width 0 \
    --qmask none \
    --dbmask none \
    --otutabout ./output/ASVtable.txt

echo Sort ASV table numerically (by decreasing ASV number)

sort -k1.4n ./output/ASVtable.txt > ./output/ASVtable.sorted.txt # -k1.4 means sorting by the 4th symbol of the first column

```
****

## 11. Searching ASVs across reference sequence database
ASV will be compared with reference sequences and results written into a table:  

| ASV | Reference | Similarity |  

First, ASVs are compared with SILVA eukaryotic SSU database to get rid of non-myxomycete sequences.  
Second, they are compared with a curated collection of myxomycete SSU sequences for a better quality species identification.

****
```bash
# Search ASV centroids across SILVA eukaryotic SSU database with no similarity threshold
vsearch --usearch_global ./output/ASVs.fasta \
    --threads $THREADS \
    --db $EUKREF.udb \
    --qmask none \
    --dbmask none \
    --id 0 \
    --top_hits_only \
    --mincols 200 \
    --maxaccepts 100 \
    --maxrejects 100 \
    --userout ./output/ASVs.euk.tsv \
    --userfields query+target+id \
    --strand both

# Add SILVA taxonomy strings to query-target-identity TSV table 
# Result: "ASVs.euk.tsv"
chmod u=rwx ./scripts/add_silva_taxonomy.py
./scripts/add_silva_taxonomy.py ./output/ASVs.euk.tsv $EUKREF

# Filter out all sequences that match to non-myxomycetes
# Result: ASVs.euk.myxonly.tsv
grep 'Myxogastria' ./output/ASVs.euk.tsv > ./output/ASVs.euk.myxonly.tsv

# Search ASV centroids across reference dataset of myxomycete SSU with no similarity threshold
vsearch --usearch_global ./output/ASVs.fasta \
    --threads $THREADS \
    --db $REF.udb \
    --qmask none \
    --dbmask none \
    --id 0 \
    --top_hits_only \
    --mincols 250 \
    --maxaccepts 100 \
    --maxrejects 100 \
    --userout ./output/ASVs.myxo.tsv \
    --userfields query+target+id \
    --strand both \
    --alnout ./output/ASVs.myxo.alignment.fasta
```
****

## 12. Final taxonomic filtering of ASVs

****
```bash
echo Filter query-target-identity TSV table produced with search across myxomycete SSU reference database according to the TSV file from the previous step
# Result: ASVs.myxo.myxonly.tsv

chmod u=rwx ./scripts/filter_tsv_by_other_tsv.py
./scripts/filter_tsv_by_other_tsv.py ./output/ASVs.euk.myxonly.tsv ./output/ASVs.myxo.tsv

# Preserve only ASVs with similarity to reference at least 60% and abundance at least 50

awk 'BEGIN {FS="[\t;=]"} $3 >= 50 && $5 >= 60' ./output/ASVs.myxo.myxonly.tsv > ./output/ASVs.myxo.myxonly.50.60.tsv

# Filter ASV table and ASV centroids according to the TSV file from the previous step
# Resulting ASV table: ASVtable.sorted.myxonly.50.60.tsv
# resulting ASV sequences: ASVs.myxonly.10.60.fasta 

chmod u=rwx ./scripts/filter_otutab_and_fasta.py
./scripts/filter_otutab_and_fasta.py ./output/ASVs.myxo.myxonly.50.60.tsv \
    ./output/ASVs.fasta \
    ./output/ASVtable.sorted.txt

# Done
```
****
